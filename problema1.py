import os
protein ={"6T26": ":Esctructura cristalina de rayos X de la fosfatasa alcalina Vibrio con el inhibidor no competitivo ciclohexilamina",
      "6PWM": ":ADC-7 en complejoo con antibiotico betalactamico ceftazidima",
      "6M2M": ":Un papel de la histona chaperona OsChz1 en el reconocimiento y la deposicion de histonas",
      "6LTW": ":Estructura cristalina de la forma Apo de I122A / I330A variante de S-adenosilmetionina sintetasa de Cryptosporidium hominis",
      "7BQL": ":The crystal structure of PdxI complex with the Alder -ene adduct " ,
      "7BQJ": ":The structure of PdxI " ,
      "7BVZ": ":Crystal structure of MreB5 of Spiroplasma citri bound to ADP ",

}

def menu():
    print(""" 
    //Menu//

    1)Agregar elementos
    2)Editar los elementos
    3)Consultar por un elemento del diccionario
    4)Eliminar un elemento
    5)Imprimir todo el diccionario
    6)Salir\n""")

    elección = input("Ingrese una opcion\n")

    if elección == "1":
        newcodigo = input("Ingrese un nuevo codigo\n")
        newcodigo = newcodigo.upper()
        descripción = input("Ingrese la descripcion del codigo ingresado\n")
        protein[newcodigo] = descripción
        print("Se ha agregado con exito!!!\n")
        menu() 

    elif elección == "2":
        codigo = input("Ingrese el codigo al cual desea cambiar su descripcion\n")
        codigo = codigo.upper()
        print(codigo)
        key = input("Ingrese la nueva descripción\n")
        protein[codigo] = key
        print("Se ha cambiado con exito!!!\n")
        menu()

    elif elección == "3":
        codigo = input("Ingrese el codigo que desea consultar\n")
        codigo = codigo.upper()
        if codigo in protein:
            print(codigo, protein[codigo])
            menu()
        else:
            print("El codigo ingresado no esta en la base de datos")
            menu()


    elif elección == "4":
        codigo = input("Ingrese el codigo que desea eliminar\n")
        codigo = codigo.upper()
        if codigo in protein:
            del protein[codigo]
            print("Se ha eliminado con exito!!")
            menu()
        else:
            print("El codigo no esta en la base de datos")
            menu()


    elif elección == "5":
        for k, v in protein.items():
            print(k, v)
        menu()


    elif elección == "6":
        os.system("clear")
        exit()


    else:
        print("La opcion ingresada no es valida\n")
        menu()

#main
menu()


